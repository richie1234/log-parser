package me.rk5.parser;

import me.rk5.parser.util.Util;
import org.apache.commons.collections.CollectionUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;



public class UtilTest {

    SparkSession spark = SparkSession.builder().appName("junit").master("local[1]")
            .config("spark.sql.warehouse.dir","file:///s3:/logs/")
            .getOrCreate();



    List<Row> inMemory = new ArrayList<>();
    Dataset<Row> dataset;
    private StructType structType;
    private Util util = new Util();

    @Before
    public void beforeTest() {
        inMemory.add(RowFactory.create("177.71.128.21", "-" ,"-", "[10/Jul/2018:22:21:28", "+0200]", "\"GET /intranet-analytics/ HTTP/1.1\"",                        "200", "3574", "-", "\"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\""));
        inMemory.add(RowFactory.create("177.71.128.21", "-" ,"-", "[10/Jul/2018:22:22:08", "+0200]", "\"GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1\"",  "200", "3574", "-", "\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));
        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:11:30", "+0200]", "\"GET http://example.net/faq/ HTTP/1.1\"",                     "200", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:10:38", "+0200]", "\"GET http://example.net/blog/category/meta/ HTTP/1.1\"",      "200", "3574", "-", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "\"GET /this/page/does/not/exist/ HTTP/1.1\"",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));


        StructField[] structFields = new StructField[] {
                new StructField("_c0",DataTypes.StringType,false,Metadata.empty()),
                new StructField("_c1",DataTypes.StringType,false,Metadata.empty()),
                new StructField("_c2",DataTypes.StringType, false, Metadata.empty()),
                new StructField("_c3",DataTypes.StringType,false,Metadata.empty()),
                new StructField("_c4",DataTypes.StringType, false, Metadata.empty()),
                new StructField("_c5",DataTypes.StringType,false,Metadata.empty()),
                new StructField("_c6",DataTypes.StringType, false, Metadata.empty()),
                new StructField("_c7",DataTypes.StringType,false,Metadata.empty()),
                new StructField("_c8",DataTypes.StringType, false, Metadata.empty()),
                new StructField("_c9",DataTypes.StringType, false, Metadata.empty()),


        };

        structType = new StructType(structFields);
        dataset = spark.createDataFrame(inMemory, structType);
        dataset.show();

    }



    @Test
    public void testFindUniqueAddressMethodShouldReturnUniqueAddress() {

        List<String>  list =  Arrays.asList("[177.71.128.21]","[168.41.191.40]", "[168.41.191.41]");
        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");
        datasetIpUrl.show();
        Dataset<Row> results = util.findUniqueAddress(datasetIpUrl);
        JavaRDD<Row> rdd = results.toJavaRDD();
        List<String>  resultsRdd =  Arrays.asList(rdd.collect().get(0).toString(),rdd.collect().get(1).toString(), rdd.collect().get(2).toString());

        assertTrue(rdd.count() == 3);
        assertTrue(CollectionUtils.isEqualCollection(resultsRdd, list));


    }


    @Test
    public void testFindUniqueAddressMethodShouldReturnUniqueAddressWhenNumOfRowsLessThan3() {
        inMemory = new ArrayList<>();
        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:10:38", "+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1",      "200", "3574", "-", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));

        dataset = spark.createDataFrame(inMemory, structType);
        dataset.show();

        List<String>  list =  Arrays.asList("[168.41.191.40]", "[168.41.191.41]");
        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");
        datasetIpUrl.show();
        Dataset<Row> results = util.findUniqueAddress(datasetIpUrl);
        JavaRDD<Row> rdd = results.toJavaRDD();
        List<String>  resultsRdd =  Arrays.asList(rdd.collect().get(0).toString(),rdd.collect().get(1).toString());


        assertTrue(rdd.count() == 2);
        assertTrue(CollectionUtils.isEqualCollection(resultsRdd, list));


    }


    @Test
    public void testFindTopMostActiveIPAddressShouldReturnCorrectIPs() {

        inMemory = new ArrayList<>();

        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:10:38", "+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1",      "200", "3574", "-", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));

        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));


        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[10/Jul/2018:22:21:28" ,"+0200]", "GET /intranet-analytics/ HTTP/1.1"                        ,"200","3574", "-" ,"\"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\""));
        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[09/Jul/2018:10:11:30" ,"+0200]", "GET http://example.net/faq/ HTTP/1.1"                     ,"200","3574", "-" ,"\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[09/Jul/2018:10:10:38" ,"+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1"      ,"200","3574", "-" ,"\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));
        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[10/Jul/2018:22:22:08" ,"+0200]", "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1"  ,"200","3574", "-" ,"\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));


        inMemory.add(RowFactory.create("177.71.128.28", "-", "-", "[11/Jul/2018:17:41:30" ,"+0200]", "GET /this/page/does/not/exist/ HTTP/1.1"                  ,"404","3574", "-" ,"\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));

        inMemory.add(RowFactory.create("177.71.128.30", "-", "-", "[10/Jul/2018:22:22:08" ,"+0200]", "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1"  ,"200","3574", "-" ,"\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));
        inMemory.add(RowFactory.create("177.71.128.30", "-", "-", "[10/Jul/2018:22:22:08" ,"+0200]", "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1"  ,"200","3574", "-" ,"\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));

        dataset = spark.createDataFrame(inMemory, structType);
        dataset.show();

        List<String>  list =  Arrays.asList("[177.71.128.21]", "[168.41.191.41]", "[177.71.128.30]");
        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");
        datasetIpUrl.show();
        Dataset<Row> results = util.findTopMostActiveIPAddress(datasetIpUrl,3);

        JavaRDD<Row> rdd = results.toJavaRDD();
        List<String>  resultsRdd =  Arrays.asList(rdd.collect().get(0).toString(),rdd.collect().get(1).toString(), rdd.collect().get(2).toString());


        assertTrue(rdd.count() == 3);
        assertTrue(CollectionUtils.isEqualCollection(resultsRdd, list));

    }

    @Test
    public void testFindTopMostActiveIPAddressShouldReturnCorrectIPsWhenNumOfRowsLessThan3() {

        inMemory = new ArrayList<>();

        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:10:38", "+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1",      "200", "3574", "-", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));

        dataset = spark.createDataFrame(inMemory, structType);
        dataset.show();

        List<String>  list =  Arrays.asList("[168.41.191.40]", "[168.41.191.41]");
        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");
        datasetIpUrl.show();
        Dataset<Row> results = util.findTopMostActiveIPAddress(datasetIpUrl,3);

        JavaRDD<Row> rdd = results.toJavaRDD();
        List<String>  resultsRdd =  Arrays.asList(rdd.collect().get(0).toString(),rdd.collect().get(1).toString());


        assertTrue(rdd.count() == 2);
        assertTrue(CollectionUtils.isEqualCollection(resultsRdd, list));

    }


    @Test
    public void testFindMostVisitedURLsShouldReturnCorrectURLs() {

        inMemory = new ArrayList<>();

        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:10:38", "+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1",      "200", "3574", "-", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));
        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[09/Jul/2018:10:10:38" ,"+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1"      ,"200","3574", "-" ,"\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));

        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /intranet-analytics/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /intranet-analytics/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));
        inMemory.add(RowFactory.create("177.71.128.24", "-", "-", "[10/Jul/2018:22:21:28" ,"+0200]", "GET /intranet-analytics/ HTTP/1.1"                        ,"200","3574", "-" ,"\"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\""));
        inMemory.add(RowFactory.create("177.71.128.22", "-", "-", "[10/Jul/2018:22:21:28" ,"+0200]", "GET /intranet-analytics/ HTTP/1.1"                        ,"200","3574", "-" ,"\"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\""));

        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));

        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[09/Jul/2018:10:11:30" ,"+0200]", "GET http://example.net/faq/ HTTP/1.1"                     ,"200","3574", "-" ,"\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));

        inMemory.add(RowFactory.create("177.71.128.21", "-", "-", "[10/Jul/2018:22:22:08" ,"+0200]", "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1"  ,"200","3574", "-" ,"\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));
        inMemory.add(RowFactory.create("177.71.128.30", "-", "-", "[10/Jul/2018:22:22:08" ,"+0200]", "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1"  ,"200","3574", "-" ,"\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));
        inMemory.add(RowFactory.create("177.71.128.30", "-", "-", "[10/Jul/2018:22:22:08" ,"+0200]", "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1"  ,"200","3574", "-" ,"\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\""));

        dataset = spark.createDataFrame(inMemory, structType);
        dataset.show();

        List<String>  list =  Arrays.asList("[GET http://example.net/blog/category/meta/ HTTP/1.1]", "[GET /intranet-analytics/ HTTP/1.1]", "[GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1]");
        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");
        datasetIpUrl.show();
        Dataset<Row> results = util.findMostVisitedURL(datasetIpUrl,3);

        JavaRDD<Row> rdd = results.toJavaRDD();
        List<String>  resultsRdd =  Arrays.asList(rdd.collect().get(0).toString(),rdd.collect().get(1).toString(), rdd.collect().get(2).toString());


        assertTrue(rdd.count() == 3);
        assertTrue(CollectionUtils.isEqualCollection(resultsRdd, list));

    }

    @Test
    public void testFindMostVisitedURLShouldReturnCorrectURLstWhenNumOfRowsLessThan3() {

        inMemory = new ArrayList<>();

        inMemory.add(RowFactory.create("168.41.191.40", "-" ,"-", "[09/Jul/2018:10:10:38", "+0200]", "GET http://example.net/blog/category/meta/ HTTP/1.1",      "200", "3574", "-", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) RockMelt/0.9.58.494 Chrome/11.0.696.71 Safari/534.24\""));
        inMemory.add(RowFactory.create("168.41.191.41", "-" ,"-", "[11/Jul/2018:17:41:30", "+0200]", "GET /this/page/does/not/exist/ HTTP/1.1",                  "404", "3574", "-", "\"Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1\""));

        dataset = spark.createDataFrame(inMemory, structType);
        dataset.show();

        List<String>  list =  Arrays.asList("[GET http://example.net/blog/category/meta/ HTTP/1.1]", "[GET /this/page/does/not/exist/ HTTP/1.1]");
        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");
        datasetIpUrl.show();
        Dataset<Row> results = util.findMostVisitedURL(datasetIpUrl,3);

        JavaRDD<Row> rdd = results.toJavaRDD();
        List<String>  resultsRdd =  Arrays.asList(rdd.collect().get(0).toString(),rdd.collect().get(1).toString());


        assertTrue(rdd.count() == 2);
        assertTrue(CollectionUtils.isEqualCollection(resultsRdd, list));

    }


}

