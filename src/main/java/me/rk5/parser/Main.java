package me.rk5.parser;

import me.rk5.parser.util.Util;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;


public class Main {

    @SuppressWarnings("resource")
    public static void main(String[] args)
    {

        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession spark = SparkSession.builder().appName("log-parser").master("local[*]")
                .config("spark.sql.warehouse.dir","file:///s3:/logs/")
                .getOrCreate();
        Util util = new Util();
        Dataset<Row> dataset;

        if (args.length == 1 ) {
            dataset = spark.read().option("delimiter", " ").csv(args[0]);
        } else {
            dataset = spark.read().option("delimiter", " ").csv("src/main/resources/programming-task-example-data.log");
        }

        Dataset<Row> datasetIpUrl = dataset.select("_c0", "_c5").toDF("ip","url");

        Dataset<Row> uniqueAddresses = util.findUniqueAddress(datasetIpUrl);
        System.out.println("List of unique addresses");
        print(uniqueAddresses);

        Dataset<Row> mostActiveIPAddresses = util.findTopMostActiveIPAddress(datasetIpUrl, 3);
        System.out.println("List of most active ip addresses");
        print(mostActiveIPAddresses);

        Dataset<Row> mostVisitedUrls = util.findMostVisitedURL(datasetIpUrl, 3);
        System.out.println("List of most visited URLs");
        print(mostVisitedUrls);

        spark.close();
    }

    private static void print(Dataset<Row> dataset) {
        List<String> listOne = dataset.as(Encoders.STRING()).collectAsList();
        for (String row: listOne) {
            System.out.println(row);

        }
        System.out.println();
    }


}
