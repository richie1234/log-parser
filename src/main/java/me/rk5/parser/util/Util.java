package me.rk5.parser.util;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.col;

public class Util {

    public Dataset<Row> findMostVisitedURL(Dataset<Row> dataset, int num) {
        Dataset<Row> newDataset = dataset.groupBy(col("url"))
                .count().sort(col("count").desc(),col("url").cast(DataTypes.StringType));
        newDataset.show();
        return newDataset.limit(num).select("url");
    }

    public Dataset<Row> findUniqueAddress(Dataset<Row> dataset) {
        Dataset<Row> newDataset = dataset.select("ip").distinct();
        newDataset.show();
        return dataset.select("ip").distinct();
    }

    public Dataset<Row> findTopMostActiveIPAddress(Dataset<Row> dataset, int num) {
        Dataset<Row> newDataset = dataset.groupBy(col("ip"))
                .count().sort(col("count").desc(),col("ip").cast(DataTypes.StringType));
        newDataset.show();
        return newDataset.limit(num).select("ip");
    }
}
